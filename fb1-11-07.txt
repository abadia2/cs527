The feedback will be ANONYMOUS; please provide honest feedback!

* Replace each underscore with an integer from 1 (very poor) to 5 (excellent)

5 1. The slides/demo were well-prepared (use of examples, animations, pictures, font size, etc.)

5 2. Presentation is well-prepared and delivered (volume, pace, enunciation, coherence, understandable...)

5 3. Presentation demonstrates knowledge/understanding of the material/tool/paper

5 4. Presenter effectively responded to questions and comments

5 5. The introduction provided a general description of the problem (and described "what" before "how")

5 6. The end provided a clear summary and conclusions

5 7. Presenter was enthusiastic (and even got me interested to read more)

5 8. My overall rating of this presentation

* Provide some feedback for the presenter to improve presentation skills (at least 2-3 sentences)

A bit of a knowledge dump, I think we could've skipped what is ctest. Otherwise, very thorough presentation and clear communication (little ums, ahhs, ors)

* Describe one technical term that you learned in this talk (2-3 sentences)

Why is test-case prioritization necessary?

Test running time is the main challenge in adopting Ctest in continuous development. It takes 97 minutes to run all Ctests for Hadoop!

So instead, we can rank tests, say via the BM25 Ranking Algorithm.