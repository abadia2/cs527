# Instruction

Unlike progress reports, you need not to list work already recorded in IDoFT (i.e. PRs, new tests, etc.)

Please do not change any given header or prompt in quote blocks.
If you would like to include more sections, please append them at the end after all the given sections.

It's ok to use small piece of code/log but please do not put lengthy code/log.
Use links to files in your `cs527` repo if necessary.

Answer the question prompts only if it applies to you.
It is fine to skip the questions not applicable to you.

# Total Effort

> Estimate your effort in terms of the number of hours you spent on the project for the entire semester.

 - 10 hours attempting to upgrade NonDex (learning source code, attempting to find correct pom versions of depenendencies that wouldn't brak Nondex)
 - 10 hours (setting up scripts to test NonDex on a repo accross multipl vms. Those scripts are linked in the previous reports)
 - 50 hours on flaky tests
    - 20 hours on https://github.com/TestingResearchIllinois/idoft/pull/1204 and https://github.com/TestingResearchIllinois/idoft/pull/1173
    - 20 hours on https://github.com/TestingResearchIllinois/idoft/pull/993, https://github.com/TestingResearchIllinois/idoft/pull/929, https://github.com/TestingResearchIllinois/idoft/pull/996
    - 10 hours https://campuswire.com/c/G7A0E96FD/feed/711 (turns out the devs fixed this and we will open a PR soon in idoft linking to this. There was no way we could've fixed this as the flakiness was an inherent design issue integral to Wildfly so any PR would've been rejected. We could've opened a trash PR, but didn't because we know they don't help anyone)
    - 5 hours on tooling, until I finally discovered how to use JVM remote debugger + maven + Nondex (https://campuswire.com/c/G7A0E96FD/feed/717)

Overall, the amount of hours I put in is short by a full 40 hours work week worth of effort. Moreover, most of the hours were put in the last week. 

# Collaborative PRs

> Please list **ALL** collaborative PRs in the format of
> `PR Link,NetID1,NetID2,NetID3...`.
> Please only list the real PR link.
>
> All collaborators must include the exactly **SAME** line.

> What did you learn through collaboration? Both technically and socially.

Sometimes it's better to collaborate and bounce ideas, other times you'll make more progress working alone.
You need to balence when you work together, versus when you work separately.

# Git

> Have you used `git bisect`? For which test(s)? What were your goal(s)?

Yes helpful for finding where the bug was introduced.

> Have you used git "pickaxe" (`git log -G` or `git log -S`)? For which tests?

Yes `git log -S` is helps for finding when a test was removed. In general, its a very powerful search.

> What other advanced git commands have you used throughout the semester?

1. `git remote -v` to ensure I am pointed to the correct repo
2. `git clean -fdx` completely clean the repo, including all output in `.nondex`

> What git commands have you used to clean up your commit history?

`rebase`, `revert`, `cherrpick`, `merge`, etc.

> Did you rebase or squash commits in your real (not IDoFT) PRs?
> For which test(s)?

No, usually I was able to sync my forked main with their main, make my changes, and have a PR open. 
I do understand, that you will have to fetch upstream changes and rebase if your branch becomes stale.

> Did you use git to record your debugging effort? What did you do?

Yes, I made temporary local branches to save debug statements and potential patches in separate branches. Then I could flip-flop between the branches to get debug logs, fail logs, and success logs.

# Project Specific 

> Did you have to open JIRA ticket(s) for your test(s)?
> Which project(s)?
> What is you JIRA account?

I had to create a JIRA account under Red Hat's organization for Wildfly PRs. My account' name is abadia2@illinois.edu.

> Did you join any other discussion platform to communicate with developers?
> Which project(s)?
> e.g. Slack, Discord, Zulip

Red Hat uses Zulip. I checked there for Wildfly

> Some projects require setting up a special testing environment.
> e.g. setting up a database, register account and set up keys
>
> Did you encounter such tests? What you did have to do?

Luckily, no. Wildfly had scripts to build and run a testsuite.

> Did you work on project(s) in another language other than Java? e.g. Kotlin, Groovy

No, just Java. Unless, XML counts.

# New things

> Bash scripts were used in many places in this course.
> Did you learn anything (new) about `coreutils`/Bash/Linux?
> How did it help your?

Not really

> What are some (new) things you learned about GitHub (including GitHub Actions and CI)?

I'm already familiar with GitHub

> Did you learn to use Java debugger? What features did you learn and use?

I learned how to use a remote JVM debugger with Maven

> Did you learn anything (new) in Java?

Wilfly has a "Decorator Hell" issue, making debugging incredibly difficult for someone not debrefied/educated ono the architecture/design of their codebase.

> Did you learn anything (new or advanced) about Maven or Gradle?

mvnDebug and maven surefire remote debugger

> What did you learn about open-source development?

It's not as scary as it sounds

# Scripts

> If you are asked to write some scripts for cleaning up IDoFT or maintaining course related things, please list them here.
>
> If you are working on more than one script, please use a `##` subsection with the same format for each script.

## Script Title

Link to repo or PR for the script: ...

https://github.com/Alex-Badia/distributed-nondex

Estimated hours spent on this script: ... 10 hours

Description: ...

Run a set of flaky tests from idoft against Nondex accros 10 vms for a repo. I didn't write the script for it to be seen by other developers... it's messy.

# Extra

> Some tests are hard to debug or fix. Some projects are hard to setup.
> 
> List all the cases where you think your effort deserves more points than
> 5 or 3 points/test. Describe what you did and estimated time spent on it.
>
> Please use a `##` subsection with the same format for each unique challenge you faced.

## Challenge 1

Estimated hours spent: 25 hours

> What is the problem? (briefly)

Wildfly uses decorators liberally. So when you click on a method, you have no idea which implementation is being used, and the stacktrace often is very misleading as to what is being called.

> If you resolved the problem, what did you do?

Learned to setup a JVM Remote Debugger with Maven Surefire Remote Debugger. This removed the need to make a bunch of unecessary print statements.

> If you resolved the problem, what did you try before reaching your final solution?

Lot's of print statements and going down the wrong rabbit hole (usually call stack of overloaded methods).

> If you did not resolved the problem, what did you try?

Response...


Estimated hours spent: 5 hours

> What is the problem? (briefly)

Setting up Apache NiFi. It required us to change the VM's configurations and Maven configurations to run successfully. Also they recently bumped their latest version to use Java 21. 

> If you resolved the problem, what did you do?

Looked through documentation and eventually found the "correct" set of configurations. Also tried to upgrade Nondex to Java 21 following the pom changes that were done during the Java 17 upgrade. Got a bunch of esoteric errors, that I could've gone down the rabbit hole of. However given our progress from the first 3 weeks trying to upgrade Nondex to Java 21 and test accross Java 17 versions, I gave up on that.

> If you resolved the problem, what did you try before reaching your final solution?

Lot's of print statements and going down the wrong rabbit hole (usually call stack of overloaded methods).

> If you did not resolved the problem, what did you try?

Response...