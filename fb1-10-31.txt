The feedback will be ANONYMOUS; please provide honest feedback!

* Replace each underscore with an integer from 1 (very poor) to 5 (excellent)

5 1. The slides/demo were well-prepared (use of examples, animations, pictures, font size, etc.)

5 2. Presentation is well-prepared and delivered (volume, pace, enunciation, coherence, understandable...)

5 3. Presentation demonstrates knowledge/understanding of the material/tool/paper

5 4. Presenter effectively responded to questions and comments

5 5. The introduction provided a general description of the problem (and described "what" before "how")

5 6. The end provided a clear summary and conclusions

5 7. Presenter was enthusiastic (and even got me interested to read more)

5 8. My overall rating of this presentation

* Provide some feedback for the presenter to improve presentation skills (at least 2-3 sentences)

I think the projector metahpor for model-based testing was really well thought out. Somethimes went too fast for the
amount of text on the slide, where I couldn'd finish processing the text on the slide before going to the next slide.

* Describe one technical term that you learned in this talk (2-3 sentences)

What is a model in model-based testing?

A model is a description of a system and an abstraction of the system it describes which help to understand and predict the system's behavior
