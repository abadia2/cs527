Feedback: Extension; I will distribute a progress report template next time; also please address Darko's comment for your IDoFT PR;
# PRs
Opened tentative PRs (with the number of test fixed):
- https://github.com/USERNAME/REPO/pull/1 (5 fixed)
- https://github.com/USERNAME/REPO/pull/2 (1 fixed)

Reviewed tentative PRs:
- https://github.com/USERNAME/REPO/pull/1
- https://github.com/USERNAME/REPO/pull/2

Opened real PRs (with the number of test fixed):
- https://github.com/USERNAME/REPO/pull/1 (2 fixed)
- https://github.com/USERNAME/REPO/pull/2 (100 fixed)

Followed up on previously opened real PRs (e.g. developers asked for adjustment):
- https://github.com/USERNAME/REPO/pull/1
- https://github.com/USERNAME/REPO/pull/2

Opened IDoFT PRs (with number and kind of updates):
- https://github.com/TestingResearchIllinois/idoft/pull/xxx (2 Opened, 3 DeveloperFixed)
- https://github.com/TestingResearchIllinois/idoft/pull/xxx (5 Deleted)

# Tentative PRs Questions
**Briefly** explain what challenges you faced or any confusion you have so that we can give you feedback.
Feel free to ignore this part if you don't have questions with your tentative PRs.

# Unfixed Test Description
**Thoroughly** explain what technical challenges you faced; what solutions you have tried; why they are not able to help.
If a test is unfixable because it's deleted/ignored/unflaky or due to similar reasons, please include it in the next section.

# Other Activities
If you spent time working on other things (e.g. improve `NonDex`, help clean up `IDoFT`, etc.), please describe what you did here.

Please also list your collaborative work here.
