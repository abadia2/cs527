# https://github.com/TestingResearchIllinois/idoft/pull/1221


## com.espertech.esper.regressionrun.suite.epl.TestSuiteEPLSubselect.testEPLSubselectNamedWindowPerformance

```shell
# Sanity Check from Idoft
git checkout master -f
mvn clean install -DskipTests -Dgpg.skip=true -Dcheckstyle.skip=true

mvn edu.illinois:nondex-maven-plugin:2.1.1:clean
mvn -pl regression-run edu.illinois:nondex-maven-plugin:2.1.1:nondex -DnondexRuns=3 -Dtest=com.espertech.esper.regressionrun.suite.epl.TestSuiteEPLSubselect#testEPLSubselectNamedWindowPerformance -DfailIfNoTests=false -Dcheckstyle.skip -Dgpg.skip=true -Dsurefire.useFile=false > flaky_test_from_idoft--nondex--surefire_2.8--commit--590fa9c9eb854f1420b9d337b802aca19f963cc0--com.espertech.esper.regressionrun.suite.epl.TestSuiteEPLSubselect.testEPLSubselectNamedWindowPerformance.log

# Sanity Check from Latest
git checkout master -f
mvn clean install -DskipTests -Dgpg.skip=true -Dcheckstyle.skip=true

mvn edu.illinois:nondex-maven-plugin:2.1.1:clean
mvn -pl regression-run edu.illinois:nondex-maven-plugin:2.1.1:nondex -DnondexRuns=10 -Dtest=com.espertech.esper.regressionrun.suite.epl.TestSuiteEPLSubselect#testEPLSubselectNamedWindowPerformance -DfailIfNoTests=false -Dcheckstyle.skip -Dgpg.skip=true > flaky_test_from_lastest--nondex--surefire_2.8--commit--e73f3b763ee3a5e4434b80d5a2e937cb2717755e--com.espertech.esper.regressionrun.suite.epl.TestSuiteEPLSubselect.testEPLSubselectNamedWindowPerformance.log
```


## com.espertech.esper.regressionrun.suite.expr.TestSuiteExprDateTime

I do not think the following 2 tests should be marked as "Developer Fixed".
  - com.espertech.esper.regressionrun.suite.expr.TestSuiteExprDateTime.testExprDTPerfIntervalOps
  - com.espertech.esper.regressionrun.suite.expr.TestSuiteExprEnum.testExprEnumNamedWindowPerformance

I believe this is a false positive. After over 100+ hundred NonDex for the entire TestSuiteExprDateTime test class and upgrading maven-surefire to 2.8 to allow individual test runs, I could not get a flaky failures on both the latest commit or the replicate the flakiness from Idoft's commit sha.

Is the flakiness platform dependent? My setup is on the WSL Ubuntu 18.04.6LTS:
```shell
root@LAPTOP-MR10D0GP:~/esper# java -version
java version "1.8.0_202"
Java(TM) SE Runtime Environment (build 1.8.0_202-b08)
Java HotSpot(TM) 64-Bit Server VM (build 25.202-b08, mixed mode)
root@LAPTOP-MR10D0GP:~/esper# mvn -version
Apache Maven 3.9.2 (c9616018c7a021c1c39be70fb2843d6f5f9b8a1c)
Maven home: /root/apache-maven-3.9.2
Java version: 1.8.0_202, vendor: Oracle Corporation, runtime: /root/jdk1.8.0_202/jre
Default locale: en, platform encoding: UTF-8
OS name: "linux", version: "5.10.16.3-microsoft-standard-wsl2", arch: "amd64", family: "unix"
root@LAPTOP-MR10D0GP:~/esper#
```

Have false positives ever been added to Idoft before? (The attached logs have smaller samples, since they can get large rather quick)

The PR that fixes these tests, doesn't make sense either...

Steps to reproduce:
```shell
# Sanity Check From Idoft
git checkout 590fa9c9eb854f1420b9d337b802aca19f963cc0 -f
mvn clean install -DskipTests -Dgpg.skip=true -Dcheckstyle.skip=true

mvn edu.illinois:nondex-maven-plugin:2.1.1:clean
mvn -pl regression-run edu.illinois:nondex-maven-plugin:2.1.1:nondex -DnondexRuns=10 -Dtest=com.espertech.esper.regressionrun.suite.expr.TestSuiteExprDateTime#testExprDTPerfIntervalOps -DfailIfNoTests=false -Dcheckstyle.skip -Dgpg.skip=true > flaky_test_from_idoft--nondex--surefire_2.8--commit--590fa9c9eb854f1420b9d337b802aca19f963cc0--com.espertech.esper.regressionrun.suite.expr.TestSuiteExprDateTime.testExprDTPerfIntervalOps.log

mvn edu.illinois:nondex-maven-plugin:2.1.1:clean
mvn -pl regression-run edu.illinois:nondex-maven-plugin:2.1.1:nondex -DnondexRuns=10 -Dtest=com.espertech.esper.regressionrun.suite.expr.TestSuiteExprEnum#testExprEnumNamedWindowPerformance -DfailIfNoTests=false -Dcheckstyle.skip -Dgpg.skip=true > flaky_test_from_idoft--nondex--surefire_2.8--commit--590fa9c9eb854f1420b9d337b802aca19f963cc0--com.espertech.esper.regressionrun.suite.expr.TestSuiteExprEnum.testExprEnumNamedWindowPerformance.log

mvn -pl regression-run edu.illinois:nondex-maven-plugin:2.1.1:nondex -DnondexRuns=10 -Dtest=com.espertech.esper.regressionrun.suite.expr.TestSuiteExprDateTime -DfailIfNoTests=false -Dcheckstyle.skip -Dgpg.skip=true > flaky_test_from_idoft--nondex--surefire_2.8--commit--590fa9c9eb854f1420b9d337b802aca19f963cc0--com.espertech.esper.regressionrun.suite.expr.TestSuiteExprDateTime.log

# Sanity Check from Latest
git checkout master -f
mvn clean install -DskipTests -Dgpg.skip=true -Dcheckstyle.skip=true

mvn edu.illinois:nondex-maven-plugin:2.1.1:clean
mvn -pl regression-run edu.illinois:nondex-maven-plugin:2.1.1:nondex -DnondexRuns=10 -Dtest=com.espertech.esper.regressionrun.suite.expr.TestSuiteExprDateTime#testExprDTPerfIntervalOps -DfailIfNoTests=false -Dcheckstyle.skip -Dgpg.skip=true > flaky_test_from_lastest--nondex--surefire_2.8--commit--e73f3b763ee3a5e4434b80d5a2e937cb2717755e--com.espertech.esper.regressionrun.suite.expr.TestSuiteExprDateTime.testExprDTPerfIntervalOps.log

mvn edu.illinois:nondex-maven-plugin:2.1.1:clean
mvn -pl regression-run edu.illinois:nondex-maven-plugin:2.1.1:nondex -DnondexRuns=10 -Dtest=com.espertech.esper.regressionrun.suite.expr.TestSuiteExprEnum#testExprEnumNamedWindowPerformance -DfailIfNoTests=false -Dcheckstyle.skip -Dgpg.skip=true > flaky_test_from_lastest--nondex--surefire_2.8--commit--e73f3b763ee3a5e4434b80d5a2e937cb2717755e--com.espertech.esper.regressionrun.suite.expr.TestSuiteExprEnum.testExprEnumNamedWindowPerformance.log

mvn -pl regression-run edu.illinois:nondex-maven-plugin:2.1.1:nondex -DnondexRuns=10 -Dtest=com.espertech.esper.regressionrun.suite.expr.TestSuiteExprDateTime -DfailIfNoTests=false -Dcheckstyle.skip -Dgpg.skip=true > flaky_test_from_latest--nondex--surefire_2.8--commit--e73f3b763ee3a5e4434b80d5a2e937cb2717755e--com.espertech.esper.regressionrun.suite.expr.TestSuiteExprDateTime.log
```



## com.espertech.esper.regressionrun.suite.infra.TestSuiteInfraNWTable.testInfraNWTableInfraFAF

See [/com.espertech.esper.regressionrun.suite.infra.TestSuiteInfraNWTable.testInfraNWTableInfraFAF](/incomplete/finalize-pull-1221/com.espertech.esper.regressionrun.suite.infra.TestSuiteInfraNWTable.testInfraNWTableInfraFAF/) for logs.

Changing `com.espertech.esper.regressionrun.suite.infra.TestSuiteInfraNWTable.testInfraNWTableInfraFAF` to "DeveloperFixed" is incorrect. 

Here is the timeline for this specific test (`com.espertech.esper.regressionrun.suite.infra.TestSuiteInfraNWTable.testInfraNWTableInfraFAF`).

(Oct 22, 2018) The test, `testInfraNWTableInfraFAF`, is first introduced:
- https://github.com/espertechinc/esper/commit/6f260c48a0317a77322b66c1b327c803acb08744#diff-0aaa74c4777ad16c0668ab0cca73bda0908d68e24dd22d110b27dabd8bac83cf

(Nov 21, 2018) Idoft references the following commit for the flaky test `testInfraNWTableInfraFAF`:
- https://github.com/espertechinc/esper/tree/590fa9c9eb854f1420b9d337b802aca19f963cc0

(May 29, 2019) The test, `testInfraNWTableInfraFAF`, is renamed to `testInfraNWTableFAF`:
- https://github.com/espertechinc/esper/commit/5a8f6f0b6daad418576024f2c9f050a61dca4660#diff-0aaa74c4777ad16c0668ab0cca73bda0908d68e24dd22d110b27dabd8bac83cf

(Dec 18, 2023) The test, `testInfraNWTableFAF` still exists in master:
- https://github.com/espertechinc/esper/blob/e73f3b763ee3a5e4434b80d5a2e937cb2717755e/regression-run/src/test/java/com/espertech/esper/regressionrun/suite/infra/TestSuiteInfraNWTable.java#L37C40-L37C40

I do not understand how `testInfraNWTableInfraFAF`  was renamed to `testInfraNWTableInfraFAFIndex`. 

The linked commit [2572a764b97241ec8b75e0697c15bc74d97a4177](https://github.com/espertechinc/esper/commit/2572a764b97241ec8b75e0697c15bc74d97a4177) has no diffs related to the renaming of `testInfraNWTableInfraFAF` to `testInfraNWTableInfraFAFIndex`.

Moreover, the renaming does not fix the test, it still fails on the master branch. Steps to reproduce:

```shell
# Sanity Check From Idoft
git checkout 590fa9c9eb854f1420b9d337b802aca19f963cc0 -f
mvn clean install -DskipTests -Dgpg.skip=true -Dcheckstyle.skip=true
mvn edu.illinois:nondex-maven-plugin:2.1.1:clean
mvn -pl regression-run edu.illinois:nondex-maven-plugin:2.1.1:nondex -DnondexRuns=10 -Dtest=com.espertech.esper.regressionrun.suite.infra.TestSuiteInfraNWTable#testInfraNWTableInfraFAF -DfailIfNoTests=false -Dcheckstyle.skip -Dgpg.skip=true > flaky_test_from_idoft_fail_nondex--surefire_2.8--commit--590fa9c9eb854f1420b9d337b802aca19f963cc0--com.espertech.esper.regressionrun.suite.infra.TestSuiteInfraNWTable.testInfraNWTableInfraFAF

# Sanity Check from Latest
git checkout e73f3b763ee3a5e4434b80d5a2e937cb2717755e -f
mvn clean install -DskipTests -Dgpg.skip=true -Dcheckstyle.skip=true
mvn edu.illinois:nondex-maven-plugin:2.1.1:clean
mvn -pl regression-run edu.illinois:nondex-maven-plugin:2.1.1:nondex -DnondexRuns=10 -Dtest=com.espertech.esper.regressionrun.suite.infra.TestSuiteInfraNWTable#testInfraNWTableFAF -DfailIfNoTests=false -Dcheckstyle.skip -Dgpg.skip=true > flaky_test_from_lastest_fails_nondex--surefire_2.8--commit--e73f3b763ee3a5e4434b80d5a2e937cb2717755e--com.espertech.esper.regressionrun.suite.infra.TestSuiteInfraNWTable.testInfraNWTableFAF
```