# https://github.com/TestingResearchIllinois/idoft/pull/1127

Following up on the unresolved PR.

## Commit With Rename
This https://github.com/apache/dubbo/pull/3806 is correct where the rename occurred here: https://github.com/apache/dubbo/pull/3806/commits/d7e11f08a6bfc85ce47c4db8dffe8c307aafc54d.

However, the diff isn't a simple renaming of a Java Class name. Instead the maintainer removed [ 
dubbo-common/src/test/java/org/apache/dubbo/common/utils/ClassHelperTest.java](https://github.com/apache/dubbo/pull/3806/commits/d7e11f08a6bfc85ce47c4db8dffe8c307aafc54d#diff-5e2e85b7e03230b78e8261caf8e53c5133c6a7bfbeba6b1251172f545ad7bfd7) and replaced it with [dubbo-common/src/test/java/org/apache/dubbo/common/utils/ClassUtilsTest.java](https://github.com/apache/dubbo/pull/3806/commits/d7e11f08a6bfc85ce47c4db8dffe8c307aafc54d#diff-4d840974e4ff821645847e2c28a495683ceb67954fe0812b7b40710e8f85bcde).

## Is org.apache.dubbo.common.utils.ClassUtilsTest.testForName3 Still OD Flaky?

After 89 rounds, it does not appear org.apache.dubbo.common.utils.ClassUtilsTest.testForName3 is still OD Flaky.
The only test verified as OD Flaky was `org.apache.dubbo.common.threadlocal.InternalThreadLocalTest#testSize()` for 5 times in a row.

So whether or not this is flaky or not is inconclusive.

_Note_: Developers sometimes commit partial changes that would obviously fail to build. This commit [d7e11f08a6bfc85ce47c4db8dffe8c307aafc54d](https://github.com/apache/dubbo/pull/3806/commits/d7e11f08a6bfc85ce47c4db8dffe8c307aafc54d), with the renaming of the test case, fails to build:

```console
...
[ERROR] Failed to execute goal org.apache.maven.plugins:maven-compiler-plugin:3.6.0:compile (default-compile) on project dubbo-config-api: Compilation failure
[ERROR] /root/dubbo/dubbo-config/dubbo-config-api/src/main/java/org/apache/dubbo/config/AbstractConfig.java:[646,26] error: cannot find symbol
[ERROR]   symbol:   method isGetter(Method)
[ERROR]   location: class ClassUtils
...
```

The next closest commit __in the PR__ that builds is: [65a9227f379f2278192b4c2d141a958d287dde55](https://github.com/apache/dubbo/pull/3806/commits/65a9227f379f2278192b4c2d141a958d287dde55) 

If you want, please read the steps to reproduce for further details...

I tried to run iDFlakies against the latest (Apache Dubbo 3.2 branch) and get this error:

```
root@LAPTOP-MR10D0GP:~/dubbo# mvn edu.illinois.cs:idflakies-maven-plugin:2.0.1-SNAPSHOT:detect -Ddetector.detector_type=random-class-method -Ddt.randomize.rounds=10 -Ddt.detector.original_order.all_must_pass=false > idflakies--10_rounds--dubbo-all--latest--f7a32be429e815e9bc70f0dec31d67461e5c695e.log
java.lang.NullPointerException
        at edu.illinois.cs.dt.tools.plugin.DetectorMojo.detectorExecute(DetectorMojo.java:287)
        at edu.illinois.cs.dt.tools.plugin.DetectorMojo.lambda$execute$5(DetectorMojo.java:211)
        at edu.illinois.cs.dt.tools.utility.ErrorLogger.runAndLogError(ErrorLogger.java:17)
        at edu.illinois.cs.dt.tools.plugin.DetectorMojo.execute(DetectorMojo.java:211)
        at org.apache.maven.plugin.DefaultBuildPluginManager.executeMojo(DefaultBuildPluginManager.java:126)
        at org.apache.maven.lifecycle.internal.MojoExecutor.doExecute2(MojoExecutor.java:342)
        at org.apache.maven.lifecycle.internal.MojoExecutor.doExecute(MojoExecutor.java:330)
        at org.apache.maven.lifecycle.internal.MojoExecutor.execute(MojoExecutor.java:213)
        at org.apache.maven.lifecycle.internal.MojoExecutor.execute(MojoExecutor.java:175)
        at org.apache.maven.lifecycle.internal.MojoExecutor.access$000(MojoExecutor.java:76)
        at org.apache.maven.lifecycle.internal.MojoExecutor$1.run(MojoExecutor.java:163)
        at org.apache.maven.plugin.DefaultMojosExecutionStrategy.execute(DefaultMojosExecutionStrategy.java:39)
        at org.apache.maven.lifecycle.internal.MojoExecutor.execute(MojoExecutor.java:160)
        at org.apache.maven.lifecycle.internal.LifecycleModuleBuilder.buildProject(LifecycleModuleBuilder.java:105)
        at org.apache.maven.lifecycle.internal.LifecycleModuleBuilder.buildProject(LifecycleModuleBuilder.java:73)
        at org.apache.maven.lifecycle.internal.builder.singlethreaded.SingleThreadedBuilder.build(SingleThreadedBuilder.java:53)
        at org.apache.maven.lifecycle.internal.LifecycleStarter.execute(LifecycleStarter.java:118)
        at org.apache.maven.DefaultMaven.doExecute(DefaultMaven.java:261)
        at org.apache.maven.DefaultMaven.doExecute(DefaultMaven.java:173)
        at org.apache.maven.DefaultMaven.execute(DefaultMaven.java:101)
        at org.apache.maven.cli.MavenCli.execute(MavenCli.java:910)
        at org.apache.maven.cli.MavenCli.doMain(MavenCli.java:283)
        at org.apache.maven.cli.MavenCli.main(MavenCli.java:206)
        at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
        at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
        at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
        at java.lang.reflect.Method.invoke(Method.java:498)
        at org.codehaus.plexus.classworlds.launcher.Launcher.launchEnhanced(Launcher.java:283)
        at org.codehaus.plexus.classworlds.launcher.Launcher.launch(Launcher.java:226)
        at org.codehaus.plexus.classworlds.launcher.Launcher.mainWithExitCode(Launcher.java:407)
        at org.codehaus.plexus.classworlds.launcher.Launcher.main(Launcher.java:348)
java.lang.NullPointerException
        at edu.illinois.cs.dt.tools.plugin.DetectorMojo.detectorExecute(DetectorMojo.java:287)
        at edu.illinois.cs.dt.tools.plugin.DetectorMojo.lambda$execute$5(DetectorMojo.java:211)
        at edu.illinois.cs.dt.tools.utility.ErrorLogger.runAndLogError(ErrorLogger.java:17)
        at edu.illinois.cs.dt.tools.plugin.DetectorMojo.execute(DetectorMojo.java:211)
        at org.apache.maven.plugin.DefaultBuildPluginManager.executeMojo(DefaultBuildPluginManager.java:126)
        at org.apache.maven.lifecycle.internal.MojoExecutor.doExecute2(MojoExecutor.java:342)
        at org.apache.maven.lifecycle.internal.MojoExecutor.doExecute(MojoExecutor.java:330)
        at org.apache.maven.lifecycle.internal.MojoExecutor.execute(MojoExecutor.java:213)
        at org.apache.maven.lifecycle.internal.MojoExecutor.execute(MojoExecutor.java:175)
        at org.apache.maven.lifecycle.internal.MojoExecutor.access$000(MojoExecutor.java:76)
        at org.apache.maven.lifecycle.internal.MojoExecutor$1.run(MojoExecutor.java:163)
        at org.apache.maven.plugin.DefaultMojosExecutionStrategy.execute(DefaultMojosExecutionStrategy.java:39)
        at org.apache.maven.lifecycle.internal.MojoExecutor.execute(MojoExecutor.java:160)
        at org.apache.maven.lifecycle.internal.LifecycleModuleBuilder.buildProject(LifecycleModuleBuilder.java:105)
        at org.apache.maven.lifecycle.internal.LifecycleModuleBuilder.buildProject(LifecycleModuleBuilder.java:73)
        at org.apache.maven.lifecycle.internal.builder.singlethreaded.SingleThreadedBuilder.build(SingleThreadedBuilder.java:53)
        at org.apache.maven.lifecycle.internal.LifecycleStarter.execute(LifecycleStarter.java:118)
        at org.apache.maven.DefaultMaven.doExecute(DefaultMaven.java:261)
        at org.apache.maven.DefaultMaven.doExecute(DefaultMaven.java:173)
        at org.apache.maven.DefaultMaven.execute(DefaultMaven.java:101)
        at org.apache.maven.cli.MavenCli.execute(MavenCli.java:910)
        at org.apache.maven.cli.MavenCli.doMain(MavenCli.java:283)
        at org.apache.maven.cli.MavenCli.main(MavenCli.java:206)
        at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
        at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
        at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
        at java.lang.reflect.Method.invoke(Method.java:498)
        at org.codehaus.plexus.classworlds.launcher.Launcher.launchEnhanced(Launcher.java:283)
        at org.codehaus.plexus.classworlds.launcher.Launcher.launch(Launcher.java:226)
        at org.codehaus.plexus.classworlds.launcher.Launcher.mainWithExitCode(Launcher.java:407)
        at org.codehaus.plexus.classworlds.launcher.Launcher.main(Launcher.java:348)
```

## Steps To Reproduce

```bash
###############################################################################
# Setup
# 
# Windows WSL2 Ubuntu 18.04.6 LTS:
###############################################################################

# Maven Version
root@LAPTOP-MR10D0GP:~/dubbo# mvn -v
Apache Maven 3.9.2 (c9616018c7a021c1c39be70fb2843d6f5f9b8a1c)
Maven home: /root/apache-maven-3.9.2
Java version: 1.8.0_202, vendor: Oracle Corporation, runtime: /root/jdk1.8.0_202/jre
Default locale: en, platform encoding: UTF-8
OS name: "linux", version: "5.10.16.3-microsoft-standard-wsl2", arch: "amd64", family: "unix"

# Java Version
root@LAPTOP-MR10D0GP:~/dubbo# java -version
java version "1.8.0_202"
Java(TM) SE Runtime Environment (build 1.8.0_202-b08)
Java HotSpot(TM) 64-Bit Server VM (build 25.202-b08, mixed mode)

###############################################################################
# iDFlakies
###############################################################################

# Sanity Check IDoft
git checkout 737f7a7ea67832d7f17517326fb2491d0a086dd7
mvn clean install -DskipTests -Dgpg.skip=true -Dcheckstyle.skip=true -U
mvn -pl dubbo-common edu.illinois.cs:idflakies-maven-plugin:2.0.1-SNAPSHOT:detect -Ddetector.detector_type=random-class-method -Ddt.randomize.rounds=100 -Ddt.detector.original_order.all_must_pass=false > idflakies--100_rounds--dubbo_common--idoft--737f7a7ea67832d7f17517326fb2491d0a086dd7.log

# Check Commit With Rename
git fetch origin 65a9227f379f2278192b4c2d141a958d287dde55
git checkout 65a9227f379f2278192b4c2d141a958d287dde55
mvn clean install -DskipTests -Dgpg.skip=true -Dcheckstyle.skip=true -U
mvn -pl dubbo-common edu.illinois.cs:idflakies-maven-plugin:2.0.1-SNAPSHOT:detect -Ddetector.detector_type=random-class-method -Ddt.randomize.rounds=100 -Ddt.detector.original_order.all_must_pass=false > idflakies--100_rounds--dubbo_common--commit_with_test_rename--65a9227f379f2278192b4c2d141a958d287dde55.log

# 
mvn -pl dubbo-common edu.illinois.cs:idflakies-maven-plugin:2.0.1-SNAPSHOT:detect -Ddetector.detector_type=random-class-method -Ddt.randomize.rounds=10 -Ddt.detector.original_order.all_must_pass=false > idflakies--10_rounds--dubbo-all--latest--f7a32be429e815e9bc70f0dec31d67461e5c695e.log

###############################################################################
# iDFlakies (Experimental Approach)
# 
# Targeting just the test class fails to reproduce the flakiness
###############################################################################

#  Try Targeting org.apache.dubbo.common.utils.ClassHelperTest
mvn clean install -DskipTests -Dcheckstyle.skip=true -U
mvn -pl dubbo-common edu.illinois.cs:idflakies-maven-plugin:2.0.1-SNAPSHOT:detect -Ddetector.detector_type=random-class-method -Ddt.randomize.rounds=10000 -Ddt.detector.original_order.all_must_pass=false -Ddt.original.order=./input.txt > idflakies--10000_rounds--dubbo_common_utils_ClassHelperTest--idoft--737f7a7ea67832d7f17517326fb2491d0a086dd7.log

# input.txt
org.apache.dubbo.common.utils.ClassHelperTest.testForName1
org.apache.dubbo.common.utils.ClassHelperTest.testForName2
org.apache.dubbo.common.utils.ClassHelperTest.testForName3
org.apache.dubbo.common.utils.ClassHelperTest.testForNameWithThreadContextClassLoader
org.apache.dubbo.common.utils.ClassHelperTest.testGetCallerClassLoader
org.apache.dubbo.common.utils.ClassHelperTest.testGetClassLoader1
org.apache.dubbo.common.utils.ClassHelperTest.testGetClassLoader2
org.apache.dubbo.common.utils.ClassHelperTest.testResolvePrimitiveClassName
org.apache.dubbo.common.utils.ClassHelperTest.testToShortString
org.apache.dubbo.common.utils.ClassHelperTest.tetForNameWithCallerClassLoader
```
