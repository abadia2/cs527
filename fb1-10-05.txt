The feedback will be ANONYMOUS; please provide honest feedback!

* Replace each underscore with an integer from 1 (very poor) to 5 (excellent)

5 1. The slides/demo were well-prepared (use of examples, animations, pictures, font size, etc.)

5 2. Presentation is well-prepared and delivered (volume, pace, enunciation, coherence, understandable...)

5 3. Presentation demonstrates knowledge/understanding of the material/tool/paper

5 4. Presenter effectively responded to questions and comments

5 5. The introduction provided a general description of the problem (and described "what" before "how")

5 6. The end provided a clear summary and conclusions

5 7. Presenter was enthusiastic (and even got me interested to read more)

5 8. My overall rating of this presentation

* Provide some feedback for the presenter to improve presentation skills (at least 2-3 sentences)

The beginning is a bit random, but I guess creates a call to action to motivate the listeners for the presentation. Overall 10 out of 10.

* Describe one technical term that you learned in this talk (2-3 sentences)

Hawaii Missile Alert (2018): The GUI was bad a lead to a bunch of false alarms. Created panic among residents and toursists.
E-commerce: testing is super important, because I guess every 100 millisecond latency leads to a 1% drop in sale for Amazon?
Scopes of UI Tests: data errors, navigational elements, element alignment, error message, cross-browsr issues.
Bunch of browser drivers, webkit, chfromium, gecko...
