Feedback: Extention; The checking done with Wildfly looks very good. However since you two worked on NonDex earlier; the point requirement would be different and I suggest to ask Darko directly if you have concerns regard how much more point-worth effort you need to put in. In terms of absolute point values: feel free to detect and fix new ID tests; also feel free to ask for other types of tests (OD/NOD/NIO...) to get the 1.4 multiplier.
# PRs (Incremental from progress 3)

If you have opened real PR for a tentative PR, only list the real PR.

Opened tentative PRs if any (with the number of test fixed): None

Opened real PRs (with the number of test fixed): None

Opened IDoFT PRs (with number and kind of updates):
- https://github.com/TestingResearchIllinois/idoft/pull/929 (6 DeveloperFixed) merged
- https://github.com/TestingResearchIllinois/idoft/pull/993 (4 DeveloperFixed) merged
- https://github.com/TestingResearchIllinois/idoft/pull/996 (4 DeveloperFixed) merged

# Summary (Cumulative)

**2-3** sentence summary of everything you did since the start of the semester,
I worked on upgrading NonDex for the first 3 weeks. Not much progress there, until the TA upgraded it very quickly.

Ritvik and I were not technically a group then, but worked on testing the NonDex for Java 17 more or less "together" until we eventually joined forces.
Then we switched to flaky tests. So far, I have found 14 flaky tests in Wildfly that are now DeveloperFixed. Our goal is to finish Wildfly and
move onto another repo quickly. Considering that I've gained 28 points in about 1.5 weeks worth of work, 
I think we're on pace. Ritvik has found a bunch of new flaky tests in Wildfly too, but I don't remember how many, but point wise, we're gaining track quickly.

I also tried to update NonDex to Java 21, but get abstruse errors. I gave up on that, but could return more towards the end of the semester. But given
our progress with NonDex at the start of the semester, let's just say "We'll put a pin in that for now..."

# Scripts (Cumulative)

If you are asked to write some scripts for cleaning up IDoFT or maintaining course related things, please list them here.

If you are working on more than one script, please have a `##` subsection for each script.

## Title (One line to describe the goal of your script)

Progress: Has your script helped you open some IDoFT PR? Are you ready to raise a PR to add your script to IDoFT? If not, what do you still need to do? Hint: some old opened PRs in IDoFT are attempts from previous semester and might be useful to you.

# Collaborative PRs (Cumulative)

Please list **ALL** of you collaborative PRs in the format of `PR Link,NetID1,NetID2,NetID3...`. If you have opened a real PR, list the 

All collaborators must include the exactly **SAME** line.

```
https://github.com/USERNAME/REPO/pull/1,NetID1,NetID2,NetID3...
```
