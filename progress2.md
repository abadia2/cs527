Feedback: Extension; Testing out one repo for one progress report is too slow. Please decide soon to switch to flaky tests or talk with us or talk with us if you would like to work on automatically find new flaky tests using your existing scripts.
DISCLAIMER: This progress2 report was uploaded LATE (on Monday 10/16/2023)

# Progress Report 2

## Cummulative Progress

Describe your cumulative progress since the start of the project, include a link to the code and tests you wrote so far, and write a plan for the next two weeks


### A Quick Recap of Our Project

We originally planned on upgrading NonDex to Java 17. After talking with Kaiyao Ke, the upgrade to Java 17 turned out to be trivial (simple pom changes).
Though a simple change, the upgrade still needs to be tested on Java 17 projects and, regressively, using Java 11 and Java 8.

Our goal was to spend the first 1-2 weeks testing the new snapshot edu.illinois:nondex-maven-plugin:2.1.7-SNAPSHOT with Java 17, Java 11, and Java 8, using the [modified-pr-data.csv](https://gist.githubusercontent.com/zzjas/2aa06ef5e7f0087496ea7ab17938b115/raw/6e2c8837607dbb44a3c4264af197d4a0230468ca/modified-pr-data.csv) from MP1.


### Our Progress

This progress2, we used the [Zzjas' forked Apache NiFi repository](https://github.com/zzjas/nifi) from the [modified-pr-data.csv](https://gist.githubusercontent.com/zzjas/2aa06ef5e7f0087496ea7ab17938b115/raw/6e2c8837607dbb44a3c4264af197d4a0230468ca/modified-pr-data.csv). The current version of Apache NiFi has a minimum version of Java 21. This fork is an older version of Apache NiFi that requires a minimum version of Java 17 -- a good candidate to test Java 17 exclusively.

In total, we were able to test the NonDex Java 17 upgrade on 73 tests (where -DnondexRuns=50) on NiFi's main branch. While some flaky tests were patched, we were able to successfully replicate others.
The upgrade plugin seems to be in good shape and don't think we really need to do any further testing.

All tests were ran automatically on a collection of virtual machines to speed up the process. The results are stored here: https://github.com/Alex-Badia/distributed-nondex/tree/nifi/logs-frozen/nifi.


# Plan for the Next 2 Weeks

- Write a plan for the next two weeks
- Document your effort if your results are not good 
- DO NOT add images and binary files to your repo (but can add links)


## Plan

Let's call this second progress report Sprint 2.

For the next sprint (Sprint 3) we will continue testing NonDex upgrade with Java 17 on other repos _if_ necessary. Depending on if Kaiyao and Zijie are satisified with our testing, we anticipate either jumping back to flaky tests or switching to a different project for Sprints 3+.
  - Week 4: Flaky Tests
  - Week 5: Flaky Tests
  - Week N: Flaky Tests
