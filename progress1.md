Good work testing NonDex 2.1.7-SNAPSHOT on Wildfly. It looks like that you are running tests manually. Have you considered automating running tests so that you only need to look into the cases that don't pass? Also, the modified-pr-data.csv is just some randomly chosen repo for MP1. You can look at the pr-data.csv in IDoFT to find more repos.

# Progress Report 1

## Cummulative Progress

Describe your cumulative progress since the start of the project, include a link to the code and tests you wrote so far, and write a plan for the next two weeks


### A Quick Recap of Our Project

We originally planned on upgrading NonDex to Java 17. After talking with Kaiyao Ke, the upgrade to Java 17 turned out to be trivial (simple pom changes).
Though a simple change, the upgrade still needs to be tested on Java 17 projects and, regressively, using Java 11 and Java 8.

Our goal was to spend the first 1-2 weeks testing the new snapshot edu.illinois:nondex-maven-plugin:2.1.7-SNAPSHOT with Java 17, Java 11, and Java 8, using the [modified-pr-data.csv](https://gist.githubusercontent.com/zzjas/2aa06ef5e7f0087496ea7ab17938b115/raw/6e2c8837607dbb44a3c4264af197d4a0230468ca/modified-pr-data.csv) from MP1.


### Our Progress

Testing the entirety of [modified-pr-data.csv](https://gist.githubusercontent.com/zzjas/2aa06ef5e7f0087496ea7ab17938b115/raw/6e2c8837607dbb44a3c4264af197d4a0230468ca/modified-pr-data.csv) is unrealistic. It is over 1200 tests,
some of which require an arbitrarily high number of NonDex runs to check flakiness. Even with multiple virtual machines, we cannot just blindly install Java 17 and Maven 3.8.1 to run all the flaky tests from all repositories.
Certain repositories, such as Hadoop, require many other dependencies, such as protobuf, to compile.

Instead, we decided to choose a sample of representitive tests. The Wildfly was a strong candidate to test edu.illinois:nondex-maven-plugin:2.1.7-SNAPSHOT with Java 17 and regressively with Java 11 since the repository supports Java 11+.
Since Wildfly repository does not require anything beyond Java and Maven to be installed, we were able to test all Wildfly (excluding "core" and "maven-plugin") flaky tests from [modified-pr-data.csv](https://gist.githubusercontent.com/zzjas/2aa06ef5e7f0087496ea7ab17938b115/raw/6e2c8837607dbb44a3c4264af197d4a0230468ca/modified-pr-data.csv). 

In total, we were able to test the NonDex Java 17 upgrade on 53 tests (where -DnondexRuns=50) on Wildfly's main branch. While some flaky tests were patched, we were able to successfully replicate others.
The Java 11 upgrade plugin seems to be in good shape, but we need to do more testing on other repos.

The results are stored here: https://github.com/Alex-Badia/distributed-nondex/tree/main/logs-frozen/wildfly.




# Plan for the Next 2 Weeks

- Write a plan for the next two weeks
- Document your effort if your results are not good 
- DO NOT add images and binary files to your repo (but can add links)


## Plan

Let's call this first progress report Sprint 1.

For the next sprint (Sprint 2) we will continue testing NonDex upgrade with Java 17 on other repos with varying versions of Java for the next 2 weeks keep Kaiyao and Zijie up to date on our progress. Since we've tested a repository that supports Java 11 and Java 17, we'r looking to test mutually exclusive versions.
  - Week 2: Verify flaky tests for a repo with only Java 17 support, we're thinking Hadoop's latest branch since they only support Java 17+ (filtering out removed or merged patches).
  - Week 3: Verify flaky tests for a repo that does not support Java 17, we're thinking about using old commits from the [modified-pr-data.csv](https://gist.githubusercontent.com/zzjas/2aa06ef5e7f0087496ea7ab17938b115/raw/6e2c8837607dbb44a3c4264af197d4a0230468ca/modified-pr-data.csv) for this.

If Kaiyao and Zijie are satisified with our testing, we anticipate either jumping back to flaky tests or switching to a different project for Sprints 3+.
  - Week 4: Flaky Tests
  - Week 5: Flaky Tests
  - Week N: Flaky Tests
