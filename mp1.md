# MP 1

## Top 10 Flaky Test From (CSV)

```shell 
wget https://gist.githubusercontent.com/zzjas/2aa06ef5e7f0087496ea7ab17938b115/raw/6e2c8837607dbb44a3c4264af197d4a0230468ca/modified-pr-data.csv

# get 10 random tests, replacing marinov@illinois.edu with your email address
grep ,ID,, modified-pr-data.csv | grep -v \\[ | shuf --random-source=<(while :; do echo abadia2@illinois.edu; done) | head
```

  1. https://github.com/zzjas/nifi,2bd752d868a8f3e36113b078bb576cf054e945e8,nifi-nar-bundles/nifi-ldap-iaa-providers-bundle/nifi-ldap-iaa-providers,org.apache.nifi.ldap.tenants.LdapUserGroupProviderTest.testSearchUsersAndGroupsMembershipThroughGroupsCaseSensitive,ID,,,
  2. https://github.com/zzjas/aem-core-wcm-components,5f29cc86b17b6db36cc617805295eb951b6504a1,bundles/core,com.adobe.cq.wcm.core.components.internal.models.v3.PageImplTest.testPage,ID,,,
  3. https://github.com/zzjas/pinot,ecf41be2ecd007853c2db19e1c6a038cf356cb9e,pinot-core,org.apache.pinot.queries.ExplainPlanQueriesTest.testSelectColumnUsingFilterOnJsonIndexColumnVerbose,ID,,,
  4. https://github.com/zzjas/hive,90fa9064f2c6907fbe6237cb46d5937eebd8ea31,standalone-metastore/metastore-server,org.apache.hadoop.hive.metastore.TestEmbeddedHiveMetaStore.testSimpleFunction,ID,,,
  5. https://github.com/zzjas/wildfly,914e190167a79c538e0f87ea4385a6626bdecc9a,marshalling/protostream,org.wildfly.clustering.marshalling.protostream.ProtoStreamUtilTestCase.testUnmodifiableMap,ID,,,Detected by Nondex 1.1.3
  6. https://github.com/zzjas/pushy,da5f8b97106cd41278b421ec63e4e76a7313bed9,pushy,com.eatthepath.pushy.apns.auth.AuthenticationTokenTest.testVerifySignature,ID,,,
  7. https://github.com/zzjas/hadoop,cc2babc1f75c93bf89a8f10da525f944c15d02ea,hadoop-tools/hadoop-distcp,org.apache.hadoop.tools.TestDistCpSystem.testPreserveUseNonEmptyDir,ID,,,
  8. https://github.com/zzjas/nifi,2bd752d868a8f3e36113b078bb576cf054e945e8,nifi-nar-bundles/nifi-ldap-iaa-providers-bundle/nifi-ldap-iaa-providers,org.apache.nifi.ldap.tenants.LdapUserGroupProviderTest.testReferencedUserUsingReferencedAttribute,ID,,,
  9. https://github.com/zzjas/flink,23c9b5ac50d04d28a34a87c78eb2d3331c06b74b,flink-runtime,org.apache.flink.runtime.state.FileStateBackendMigrationTest.testKeyedMapStateStateMigration,ID,,,
  10. https://github.com/zzjas/pinot,98c536ee80516effc6fe2df642a07c8b05962532,pinot-core,org.apache.pinot.queries.ExplainPlanQueriesTest.testSelectAggregateUsingFilterOnTextIndexColumn,ID,,,https://github.com/TestingResearchIllinois/idoft/issues/741

## Fixing Flaky Tests
### 1. nifi

To build the latest version of Apache NiFi requires Java 17. 

The minimum version to build Apache NiFi was changed from Java 8 to Java 17 (skipping Java 11) in Jira [NIFI-11717](https://issues.apache.org/jira/browse/NIFI-11717), commit [b6b621bf9e099e8bd32365e72495a6e8e6c0c52e](https://github.com/apache/nifi/commit/b6b621bf9e099e8bd32365e72495a6e8e6c0c52e), and GitHub [PR 7397](https://github.com/apache/nifi/pull/7397). 

Similarly the minimum Java Version was raised from Java 8 to Java 11 from the Jira [NIFI-11164](https://issues.apache.org/jira/browse/NIFI-11164), commit [18ef2a57a58f995994f519d8911b31a20aa01b6a](https://github.com/apache/nifi/commit/18ef2a57a58f995994f519d8911b31a20aa01b6a) and branch [drop-java-8](https://github.com/pvillard31/nifi/tree/drop-java-8).

However, the commit [18ef2a57a58f995994f519d8911b31a20aa01b6a](https://github.com/apache/nifi/commit/18ef2a57a58f995994f519d8911b31a20aa01b6a) requires Java 11.0.16, and Java 11.0.2 will not work: `JDK version 11.0.2 (JAVA_HOME=C:\Program Files\Java\jdk-11.0.2) is not in the allowed range [11.0.16,)`. 

I changed to java 11.0.16 on my machine, but the project will not build out of the box due to a classpath error with one of the test classes not being able to find test utility class.

I went back further to a commit [af3375669c374ab0ca703aee42d4ae32cea10efc](https://github.com/apache/nifi/commit/af3375669c374ab0ca703aee42d4ae32cea10efc) where the minimum version was set to Java 8.

This runs into more errors. Will come back to this one.


### 2. aem-core-wcm-components

The general workflow performed was:

```bash
git clone -c core.longpaths=true https://github.com/adobe/aem-core-wcm-components.git
cd aem-core-wcm-components

# compile the module where the test is, and all modules it depends on, but skip tests...
mvn install -pl bundles/core -am -DskipTests

# run the test to check that it passes (note the change of last '.' to '#')...
mvn clean -pl bundles/core test -D'test=com.adobe.cq.wcm.core.components.internal.models.v3.PageImplTest#testPage'

# if the test passes in regular runs, see if it fails with the NonDex tool that reports flaky tests...
mvn clean -PautoInstallPackage -pl bundles/core edu.illinois:nondex-maven-plugin:2.1.1:nondex -D'test=com.adobe.cq.wcm.core.components.internal.models.v3.PageImplTest#testPage' -D'spotbugs.skip=true'
```


Link to PR: https://github.com/alexbadia1/aem-core-wcm-components/pull/1

| Q                        | A <!--(Can use an emoji 👍) -->
| ------------------------ | ---
| Fixed Issues?            | 
| Patch: Bug Fix?          | Yes
| Minor: New Feature?      |
| Major: Breaking Change?  |
| Tests Added + Pass?      | 
| Documentation Provided   | 
| Any Dependency Changes?  | Yes (org.skyscreamer.jsonassert 1.3.0)
| License                  | Apache License, Version 2.0

<!-- Describe your changes below in as much detail as possible -->

The following `testJSONExport` method, performs a flaky comparison of JSON. 

```java
public static void testJSONExport(Object model, String expectedJsonResource) {
    Writer writer = new StringWriter();
    ObjectMapper mapper = new ObjectMapper();
    PageModuleProvider pageModuleProvider = new PageModuleProvider();
    mapper.registerModule(pageModuleProvider.getModule());
    DefaultMethodSkippingModuleProvider defaultMethodSkippingModuleProvider = new DefaultMethodSkippingModuleProvider();
    mapper.registerModule(defaultMethodSkippingModuleProvider.getModule());
    try {
        mapper.writer().writeValue(writer, model);
    } catch (IOException e) {
        fail(String.format("Unable to generate JSON export for model %s: %s", model.getClass().getName(), e.getMessage()));
    }
    JsonReader outputReader = Json.createReader(IOUtils.toInputStream(writer.toString(), StandardCharsets.UTF_8));
    InputStream is = Utils.class.getResourceAsStream(expectedJsonResource);
    if (is != null) {
        JsonReader expectedReader = Json.createReader(is);
        assertEquals(expectedReader.read(), outputReader.read());
    } else {
        fail("Unable to find test file " + expectedJsonResource + ".");
    }
    IOUtils.closeQuietly(is);
}
```

The comparison (`assertEquals(expectedReader.read(), outputReader.read());`) relies on the order of arrays in the JSON. Specifically, the testPage test will fail in some case if the order of `xdm:tags` does match the order of tags in the expected JSON file:
```json
"xdm:tags": [
  "three",
  "one",
  "two"
],
```

To check that the exported JSON is correct (without relying on array order) in `mapper.writer().writeValue(writer, model);`, the `assertEquals(expectedReader.read(), outputReader.read());` is replaced with:
```java
try {
    JsonReader expectedReader = Json.createReader(is);
    JSONAssert.assertEquals(expectedReader.read().toString(), outputReader.read().toString(), false);
} catch (JSONException jse) {
    fail("Exported JSON does not match expected JSON at " + expectedJsonResource + ".");
}
```

This change introduces the following dependency:
```xml
<dependency>
    <groupId>org.skyscreamer</groupId>
    <artifactId>jsonassert</artifactId>
    <version>1.3.0</version>
    <scope>test</scope>
</dependency>
```

Fixing this flaky utility method actually fixes multiple tests discovered by NonDex:
  - https://github.com/zzjas/aem-core-wcm-components	5f29cc86b17b6db36cc617805295eb951b6504a1	bundles/core	com.adobe.cq.wcm.core.components.internal.models.v2.PageImplTest.testPage	ID
  - https://github.com/zzjas/aem-core-wcm-components	5f29cc86b17b6db36cc617805295eb951b6504a1	bundles/core	com.adobe.cq.wcm.core.components.internal.models.v2.PageImplTest.testPageWithDeprecatedCaconfig	ID
  - https://github.com/zzjas/aem-core-wcm-components	5f29cc86b17b6db36cc617805295eb951b6504a1	bundles/core	com.adobe.cq.wcm.core.components.internal.models.v3.PageImplTest.testPage	ID
  - https://github.com/zzjas/aem-core-wcm-components	5f29cc86b17b6db36cc617805295eb951b6504a1	bundles/core	com.adobe.cq.wcm.core.components.internal.models.v3.PageImplTest.testPageWithDeprecatedCaconfig	ID

Alternatively, [uk.org.webcompere.ModelAssert](https://mvnrepository.com/artifact/uk.org.webcompere/ModelAssert/1.0.0) library can be used, or implementing a JSON string compare helper function.
