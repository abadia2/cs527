# MP 2

## Top 10 Tests

1. 4315    "org.apache.hadoop.fs.viewfs.TestViewFsLocalFs#testResolvePathThroughMountPoints": [
2. 3763    "org.apache.hadoop.fs.viewfs.TestViewFsLocalFs#testInternalRemoveXAttr": [
3. 53082    "org.apache.hadoop.fs.TestFileSystemCaching#testCacheDisabled": [
4. 43730    "org.apache.hadoop.fs.TestRawLocalFileSystemContract#testDeleteRecursively": [
5. 49019    "org.apache.hadoop.security.alias.TestCredentialProviderFactory#testJksProvider": [
6. 42962    "org.apache.hadoop.fs.TestRawLocalFileSystemContract#testRenameFileMoveToNonExistentDirectory": [
7. 48999    "org.apache.hadoop.security.alias.TestCredentialProviderFactory#testLocalBCFKSProvider": [
8. 14949    "org.apache.hadoop.fs.viewfs.TestViewFsWithAuthorityLocalFs#testInternalGetXAttr": [
9. 10907    "org.apache.hadoop.fs.viewfs.TestViewFileSystemLocalFileSystem#testInternalRenameFromSlash": [
10. 10961    "org.apache.hadoop.fs.viewfs.TestViewFileSystemLocalFileSystem#testInternalModifyAclEntries": [

## Small Task

### Command 1

```json
"python3 run_single_ctest.py org.apache.hadoop.fs.contract.sftp.TestSFTPContractSeek#testReadAtExactEOF hadoop.security.groups.cache.secs=-100": { 
  "coveredmethods":"357" 
}
```

### Command 2

```json
"python3 run_single_ctest.py org.apache.hadoop.fs.contract.sftp.TestSFTPContractSeek#testReadAtExactEOF hadoop.security.groups.cache.secs=300": { 
  "coveredmethods":"406"
}
```

##  TASK3: (False Negative or Inconsistency)

