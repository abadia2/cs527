# Draft Proposals

## Problem

## Instructions
Draft Project Proposal DUE by 23:59:59pm Champaign time
Push to your netid/cs527 Git repo a file named draft-proposal.md with these items:

Problem: <up to 80 words describing the problem you want to work on>
Proposed solution: <up to 80 words describing how you want to solve it>
Evaluation: <up to 80 words describing how you will evaluate it>
[Optional] Notes: <can provide more info/background if needed>
[Optional] Related paper: <can list one conference paper related to your project, not among those already covered>
Note: all course projects have to use open-source code.

### Problem

The problem I will be working on is upgprading Nondex from Java 11 to Java 17.

## Proposed Solutions

The plan is to upgrade nondex by major versions until something breaks. If I can't debug it, I will drop versions (major or minor) to get to a
working version. I wll have to cross reference version upgrades with the Java release notes. If I must, I may have to re-write some parts
of the code as well if there are any deprecations.

## Related Paper

N/A