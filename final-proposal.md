
## Problem

Regression test NonDex Java 17 Upgrades [Was upgrade NonDex to work with Java 17]

## Proposed Solution

I will use NonDex with Java 17 on open source code using older java version (regression test). Then
I will use the newly upgraded NonDex to work with newer flaky tests using Java 17. After, I will default to
fixing flaky tests. Ritivik and I talked with the TA about this, but are also look for new ideas to pursue.

## Evaluatiom

The evaulation pretty much follows MP1, for fixing flaky tests. There is an additional code review pull requests component 
mentioned in recent lectures.

## Initial results: 

We read through the NonDex research papers to understand Nondex from a high level. We scheduled a meeting with a TA to 
learn more about NonDex and what was done to go from Java 8 to Java 11. After meeting with the TA, it turns out upgrading
NonDex to Java 17 is just some simple POM file changes.

## Immediate next steps

Since the upgrading NonDex involves a few trivial POM line changes, this task is too short to do aas a final project. Thus,
we will have to look for a different final project. Given that this was very close to the deadline, I am opting to just test NonDex
on some new repos with Java 17 and perform some regression testing. After that, I will default to flaky tests, until I find a new project.