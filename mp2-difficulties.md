# Difficulty 2: Long Running CTests

A Ctest take about 1 to 3 minutes to execute. Many students in this class wrote scripts that ran for a few hours to generate all the test results.

However, having already taken a parallel programming course this problem strikes me as embarresingly parallel. The ctests could be run in parallel.
The only caveat is the single clover.xml that is generated and read from per ctest. This concurrnency problem can be solved by creating hashed clover.xml per run. I think this would require modifying how the clover.xml works.

Combine this with the fact that I am in distributed systems, and I think this would be a very easyproblem to solve with map reduce. In the map, we run the build, and in the reduce we do the post processing to create the mp2.csv.

To bring a set of 156 ctests down to 15 minutes, I wrote a parallel program in Python using the multiprocessing module. The input was a JSON of tests, their properties, and the properties type, default, allowed_values, bad_values, relied on props, etc.

The JSON is then parsed into a series of commands to run a ctest. Each series of commands to run a ctest is dumped into a concurrent safe queue where a pool of processes continuously poll for ctests to run.

This helps guarentee an efficient use of each VM, since compilation times can vary. Each process was assigned a VM and ran the commands, capturing all output.

Then the final step was parsing all the logs (concurrently of course) and entering them into a CSV.


I'd be very interested in applying distributed techniques to speed up builds or test suites.


# Difficulty 2: Extremely Outdated Package Manager

Imagine working at a company who is still using Bazel 0.x.x. Yes you hear that, in the 3+ years of development, no thought of upgrading Bazel... and when Bazel was already nearing 4.x.x.

Upgrading from an older version of Bazel (such as Bazel 0.x.x) to the latest version in a 3+ year-old codebase can be a challenging and time-consuming process. Bazel is a powerful build tool, but it also evolves rapidly, which means that upgrading may involve addressing various compatibility issues, build configuration changes, and updates to your codebase. Some of the difficulties I encountered were:

  1. __Compatibility Issues__: Changes in Bazel's architecture, rules, and syntax. Older build files and build rules were not compatible with the latest Bazel version. This mean lots of build failures, compilation errors, warnings, and deprecations, etc.

  2. __API Changes__: Needed to update or replace custom build rules that became obsolete. So lot's of refactoring and testing to ensure that your build process remains functional.

  3. __Workspace Configuration__: build toolchain configurations changed due to "latest best practices". 

  4. __Dependency Updates__: resoloving dependencies was difficult. Again, things broke, warnings everywhere, etc.

  5. __Build Performance__: Optimized builds are no longer optimized when upgrading. The old version took 2 hours for a pipeline.

  6. __Testing__: Lot's of re-running tests.

  7. __Plugin Compatibility_: Some extensions with Bazel broke and some were irreplaceable which meant redesign time

  8. __Documentation__: Our internal documentationw as already useless, this made it even more obsolete.

  9. __Integration with CI/CD__: I didn't have to deal with this, but the DevOps did. Pipelines broke a lot.


So while no unque approach was taken here, I believe there is great research potential in upgrading legacy systems. We essentially took the brute force approach as we had no other internal tools to accelrate the process. However, having a system that given a tech stack could cross reference old documentation, apis, plugins, dependencies, etc of old systems to the current stuff would be cool.
